(use-modules (ice-9 nice-9)
             (srfi srfi-1)
             (pamphlet))

#;(define (sum numbers)
(if (eq? numbers ’())
    0
    (+ (car numbers) (sum (cdr numbers)))))

#;(define (map f l)
(if (eq? l ’())
    ’()
     (cons (f (car l)) (map f (cdr l)))))

(define (square number)
  (* number number))


(define (squares numbers)
  (if (eq? numbers '())
      '()
      (cons (square (car numbers)) (squares (cdr numbers)))))


(define ((divides? a) b)
  (= (remainder a b) 0))


(define (divisors n from)
  (if (> from n)
      '()
      (if (= (remainder n from) 0)
          (cons from (divisors n (+ from 1)))
          (divisors n (+ from 1)))))


(define (prime? number)
  (equal? (divisors number 1) (list 1 number)))


(define (prime-numbers amount from)
  (if (= amount 0)
      '()
       (if (prime? from)
           (cons from (prime-numbers (- amount 1) (+ from 1)))
           (prime-numbers amount (+ from 1)))))

;; Wow! This is a neat little trick to annotate what an argument
;; actually stands for, when it is not a keyword argument!
(apply + (map square (prime-numbers 7 #;greater-than-or-equal 1)))
