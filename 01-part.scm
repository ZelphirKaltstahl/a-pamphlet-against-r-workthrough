(use-modules (ice-9 nice-9)
             (srfi srfi-1)
             (pamphlet))

;; (define (lookup key #;in mapping)
;;   (let* ((this (car mapping))
;;          (remaining (cdr mapping))
;;          (name (car this))
;;          (value (cdr this)))
;;     (if (eq? name key)
;;         value
;;         (lookup key remaining))))

(define (lookup key #;in mapping)
  ;; Huh, I did not know we could pattern match inside let* o.0
  (let* ([((name . value) . remaining) mapping])
    (if (eq? name key)
        value
        (lookup key remaining))))

(define alist-lookup lookup)


(define (satisfied? formula #;under valuation)
  ;; match is from (ice-9 nice-9)
  ;; For more see:
  ;; https://www.gnu.org/software/guile/manual/html_node/Pattern-Matching.html
  ;; Possibly we could also get it from (ice-9 match).
  (match formula
    [('and . clauses)
     (every (lambda (clause)
              (satisfied? clause #;under valuation))
            clauses)]
    [('or . clauses)
     (any (lambda (clause)
            (satisfied? clause #;under valuation))
          clauses)]
    [('not clause)
     (not (satisfied? clause #;under valuation))]
    [(? symbol?)
     (lookup formula #;in valuation)]))
